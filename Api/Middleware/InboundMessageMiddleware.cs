﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Zhant.Wrike.Integration.GitLab.Api.Extensions;

namespace Zhant.Wrike.Integration.GitLab.Api.Middleware
{
    public abstract class InboundMessageMiddleware
    {
        private readonly RequestDelegate _next;

        public InboundMessageMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            var request = context.Request;
            var correlationId = $"{DateTime.Now.Ticks}{Thread.CurrentThread.ManagedThreadId}";
            var requestInfo = $"{request.Method} {request.GetDisplayUrl()}";
            context.Items.Add("correlationId", correlationId);
            context.Items.Add("requestInfo", requestInfo);
            var requestMessage = request.ReadAsText();
            await IncomingMessageAsync(correlationId, requestInfo, await requestMessage);

            await _next(context);
        }

        protected abstract Task IncomingMessageAsync(string correlationId, string requestInfo, string message);
    }
}