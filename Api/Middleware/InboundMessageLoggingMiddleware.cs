﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Serilog;

namespace Zhant.Wrike.Integration.GitLab.Api.Middleware
{
    public class InboundMessageLoggingMiddleware : InboundMessageMiddleware
    {
        public InboundMessageLoggingMiddleware(RequestDelegate next) : base(next)
        {
        }

        protected override async Task IncomingMessageAsync(string correlationId, string requestInfo, string message)
        {
            await Task.Run(() => Log.Information("{@correlationId} - Request: {@requestInfo}\r\n{@message}", correlationId, requestInfo, message));
        }
    }
}