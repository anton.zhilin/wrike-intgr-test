﻿using System.IO;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Zhant.Wrike.Integration.GitLab.Api.Extensions;

namespace Zhant.Wrike.Integration.GitLab.Api.Middleware
{
    public abstract class OutboundMessageMiddleware
    {
        private readonly RequestDelegate _next;

        public OutboundMessageMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            var response = context.Response;
            var originalBodyStream = response.Body;
            using (var responseBody = new MemoryStream())
            {
                response.Body = responseBody;

                await _next(context);

                var responseMessage = response.ReadAsText().ConfigureAwait(false);
                var correlationId = context.Items["correlationId"] as string;
                var requestInfo = context.Items["requestInfo"] as string;
                await OutgoingMessageAsync(correlationId, requestInfo, (HttpStatusCode)response.StatusCode, await responseMessage);
                await responseBody.CopyToAsync(originalBodyStream);
            }
        }

        protected abstract Task OutgoingMessageAsync(string correlationId, string requestInfo, HttpStatusCode statusCode, string message);
    }
}