﻿using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Serilog;

namespace Zhant.Wrike.Integration.GitLab.Api.Middleware
{
    public class OutboundMessageLoggingMiddleware : OutboundMessageMiddleware
    {
        public OutboundMessageLoggingMiddleware(RequestDelegate next) : base(next)
        {
        }

        protected override async Task OutgoingMessageAsync(string correlationId, string requestInfo, HttpStatusCode statusCode, string message)
        {
            await Task.Run(() => Log.Information("{@correlationId} - Response: {@requestInfo} - {@intHttpStatusCode} {@httpStatusCode}\r\n{@message}",
                correlationId, requestInfo, statusCode, statusCode, message));
        }
    }
}