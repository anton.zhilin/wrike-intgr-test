﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;

namespace Zhant.Wrike.Integration.GitLab.Api.Middleware
{
    public class EnableRequestRewind
    {
        private readonly RequestDelegate _next;

        public EnableRequestRewind(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            context.Request.EnableRewind();
            await _next(context);
        }
    }
}