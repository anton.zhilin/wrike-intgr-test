using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using Autofac;
using Microsoft.Extensions.Configuration;
using NGitLab;
using Taviloglu.Wrike.ApiClient;
using Taviloglu.Wrike.Core;
using Zhant.Wrike.Integration.GitLab.Core;
using Zhant.Wrike.Integration.GitLab.Core.Clients;
using Zhant.Wrike.Integration.GitLab.Core.Model.Sync;
using Zhant.Wrike.Integration.GitLab.Core.Store;

namespace Zhant.Wrike.Integration.GitLab.Api.Configuration
{    
    public class ConfigurationModule : Module
    {
        private readonly IConfigurationRoot _configuration;
        public ConfigurationModule(IConfigurationRoot configuration)
        {
            _configuration = configuration;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterInstance(GitLabClientInstance()).As<IGitLabClient>();
            builder.RegisterType<GitLabNoteClient>().WithParameter("client", CreateGitLabNoteHttpClient()).As<IGitLabNoteClient>();
            builder.RegisterInstance(WrikeClientInstance()).AsSelf();

            var readStore = new ReadStore();
            builder.RegisterInstance(readStore).As<IReadFromStore>();
            builder.RegisterInstance(new WriteStore()).As<IWriteToStore>();

/*            IEnumerable<ProjectSynchronization> projectSynchronizations = readStore.GetAll<ProjectSynchronization>();
            ISynchronizationMappings<ProjectSynchronization> projectSynchronizationMappings = new SynchronizationMappings<ProjectSynchronization>(projectSynchronizations);
//            builder.RegisterInstance(taskProvider).AsSelf();

            var synchronizationMappings = readStore.GetAll<SynchronizationMap>();
            var synchronizationMap = new SynchronizationMappings<SynchronizationMap>(synchronizationMappings);

            SynchronizationProvider<TaskSyncable> taskProvider = new SynchronizationProvider<TaskSyncable>(synchronizationMap);
            var commentProvider = new SynchronizationProvider<CommentSyncable>(synchronizationMap);
            builder.RegisterInstance(taskProvider).AsSelf();
            builder.RegisterInstance(commentProvider).AsSelf();*/

        }

        private HttpClient CreateGitLabNoteHttpClient()
        {
            var client = new HttpClient {BaseAddress = new Uri(_configuration["GitLab:ApiUrl"]) };
            client.DefaultRequestHeaders.Add("PRIVATE-TOKEN", _configuration["GitLab:PrivateKey"]);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            return client;
        }

        private WrikeClient WrikeClientInstance() 
        {
            return new WrikeClient(_configuration["Wrike:token"]);
        }

        private GitLabClient GitLabClientInstance()
        {
            return new GitLabClient(_configuration["GitLab:ApiUrl"], _configuration["GitLab:PrivateKey"]);
        }
    }
}
