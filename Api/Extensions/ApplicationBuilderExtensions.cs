﻿using System;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;
using Zhant.Wrike.Integration.GitLab.Api.Exceptions;
using Zhant.Wrike.Integration.GitLab.Api.Filters;

namespace Zhant.Wrike.Integration.GitLab.Api.Extensions
{
    public static class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder UseSerilog(this IApplicationBuilder app, ILoggerFactory loggerFactory)
        {
            var configuration = new LoggerConfiguration();
            Log.Logger = configuration.CreateLogger();
            loggerFactory.AddSerilog();
            return app;
        }

        public static IServiceProvider UseTestEnabledRegistration(this IServiceCollection services, IHostingEnvironment environment, IConfiguration configuration, Action<ContainerBuilder> explicitRegistration = null)
        {
            var builder = new ContainerBuilder();
            services.AddMvc(config =>
                {
                    // TODO                config.Filters.Add(new RequireHttpsAttribute());
                    config.Filters.Add(new ExceptionHandlerFilterAttribute());
                    config.Filters.Add(new GlobalExceptionLogger());
                })./*AddJsonOptions(JsonSerializerOptions()).*/
                AddFluentValidation(validation => validation.RegisterValidatorsFromAssemblyContaining<Startup>());

            builder.Populate(services);

            var container = builder.Build();
            return new AutofacServiceProvider(container);
        }
    } 
}