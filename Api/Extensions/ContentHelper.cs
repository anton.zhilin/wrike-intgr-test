﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Zhant.Wrike.Integration.GitLab.Api.Extensions
{
    public static class ContentHelper
    {
        public static async Task<string> ReadAsText(this HttpRequest request)
        {
            var body = request.Body;
            var buffer = new byte[Convert.ToInt32(request.ContentLength)];
            await request.Body.ReadAsync(buffer, 0, buffer.Length);
            body.Seek(0, SeekOrigin.Begin);
            request.Body = body;
            return Encoding.UTF8.GetString(buffer);
        }

        public static async Task<string> ReadAsText(this HttpResponse response)
        {
            response.Body.Seek(0, SeekOrigin.Begin);
            var responseMessage = await new StreamReader(response.Body).ReadToEndAsync();
            response.Body.Seek(0, SeekOrigin.Begin);
            return responseMessage;
        }

    }
}