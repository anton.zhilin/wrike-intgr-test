using System;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Zhant.Wrike.Integration.GitLab.Api.Configuration;
using Zhant.Wrike.Integration.GitLab.Api.Exceptions;
using Zhant.Wrike.Integration.GitLab.Api.Extensions;
using Zhant.Wrike.Integration.GitLab.Api.Filters;
using Zhant.Wrike.Integration.GitLab.Api.Middleware;

namespace Zhant.Wrike.Integration.GitLab.Api
{
    public class Startup
    {
        private IHostingEnvironment CurrentEnvironment { get; set; }
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
            CurrentEnvironment = env;
        }

        public IConfigurationRoot Configuration { get; private set; }
        public IContainer ApplicationContainer { get; private set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            var builder = new ContainerBuilder();
            services.AddMvc(config =>
            {
                config.Filters.Add(new ExceptionHandlerFilterAttribute());
                config.Filters.Add(new GlobalExceptionLogger());
            }).AddFluentValidation(validation => validation.RegisterValidatorsFromAssemblyContaining<Startup>());

            builder.Populate(services);

            builder.RegisterModule(new ConfigurationModule(Configuration));
            ApplicationContainer = builder.Build();

            return new AutofacServiceProvider(ApplicationContainer);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, IApplicationLifetime appLifetime)
        {
            app.UseSerilog(loggerFactory);
            app.UseMiddleware<EnableRequestRewind>();
            app.UseMiddleware<InboundMessageLoggingMiddleware>();
            app.UseMiddleware<OutboundMessageLoggingMiddleware>();

            /*if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }*/

            app.UseAuthentication();
            app.UseMvc();            
            appLifetime.ApplicationStopped.Register(() => ApplicationContainer.Dispose());
        }
    }
}
