﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Zhant.Wrike.Integration.GitLab.Api.Filters
{
    public class NotNullAttribute : ActionFilterAttribute
    {
        public const string ExceptionMessage = "No values were provided. We can't process your request.";

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));

            foreach (var parameter in context.ActionDescriptor.Parameters)
            {
                context.ActionArguments.TryGetValue(parameter.Name, out var value);
                if (value == null)
                    context.ModelState.AddModelError(parameter.Name, ExceptionMessage);
            }

            if (!context.ModelState.IsValid)
                context.Result = new BadRequestObjectResult(context.ModelState);

            base.OnActionExecuting(context);
        }
    }

}