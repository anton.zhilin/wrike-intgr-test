﻿using Microsoft.AspNetCore.Mvc.Filters;
using Serilog;

namespace Zhant.Wrike.Integration.GitLab.Api.Filters
{
    public class ExceptionHandlerFilterAttribute : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            Log.Error(context.Exception, "Unhandled exception");
            base.OnException(context);
        }
    }
}