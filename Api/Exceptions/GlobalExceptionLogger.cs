﻿using Microsoft.AspNetCore.Mvc.Filters;
using Serilog;

namespace Zhant.Wrike.Integration.GitLab.Api.Exceptions
{
    public class GlobalExceptionLogger : IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            Log.Error(context.Exception, "Unhandled exception");
        }
    }
}