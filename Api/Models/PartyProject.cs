﻿namespace Zhant.Wrike.Integration.GitLab.Api.Models
{
    public class PartyProject
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}