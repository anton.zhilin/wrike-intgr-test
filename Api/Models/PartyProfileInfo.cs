﻿using System.Collections.Generic;
using Zhant.Wrike.Integration.GitLab.Api.Controllers;

namespace Zhant.Wrike.Integration.GitLab.Api.Models
{
    public class PartyProfileInfo
    {
        public string UserInfo { get; set; }
        public IEnumerable<PartyProject>  UserProjects { get; set; }
    }
}