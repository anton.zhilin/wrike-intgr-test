﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NGitLab;
using Serilog;
using Taviloglu.Wrike.ApiClient;
using Zhant.Wrike.Integration.GitLab.Core;
using Zhant.Wrike.Integration.GitLab.Core.Clients;
using Zhant.Wrike.Integration.GitLab.Core.Extensions;
using Zhant.Wrike.Integration.GitLab.Core.Model.Sync;
using Zhant.Wrike.Integration.GitLab.Core.Store;

namespace Zhant.Wrike.Integration.GitLab.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class SynchronizationsController : Controller
    {
        private readonly WrikeClient _wrikeClient;
        private readonly IGitLabClient _gitLabClient;
        private readonly IGitLabNoteClient _gitLabNotesClient;
        private readonly IWriteToStore _writeToStore;
        private readonly ISynchronizationMappings<SynchronizationMap> _mappings;
        private readonly SynchronizationMappings<ProjectSynchronization> _projectSynchronizationMappings;

        public SynchronizationsController(WrikeClient wrikeClient, IGitLabClient gitLabClient, IGitLabNoteClient gitLabNotesClient,
            IReadFromStore readFromStore, IWriteToStore writeToStore)
        {
            _gitLabClient = gitLabClient;
            _wrikeClient = wrikeClient;
            _writeToStore = writeToStore;
            _gitLabNotesClient = gitLabNotesClient;

            var projectSynchronizations = readFromStore.GetAll<ProjectSynchronization>().ToList();
            _projectSynchronizationMappings = new SynchronizationMappings<ProjectSynchronization>(projectSynchronizations);
            var synchronizationMaps = readFromStore.GetAll<SynchronizationMap>();
            _mappings = new SynchronizationMappings<SynchronizationMap>(synchronizationMaps);
        }


        [HttpGet]
        [Route("")]
        public IActionResult GetForPartyAndWrike([FromQuery]string wrikeId, [FromQuery]string partnerId)
        {
            if (string.IsNullOrEmpty(wrikeId) && string.IsNullOrEmpty(partnerId))
                return Ok(_projectSynchronizationMappings.GetMappings<ProjectSynchronization>());

            var projectSynchronization = _projectSynchronizationMappings.GetMappings<ProjectSynchronization>().FirstOrDefault(x => x.WrikeId == wrikeId && x.PartnerId == partnerId);
            return projectSynchronization != null ? Ok(projectSynchronization) : (IActionResult) NotFound();
        }

        [HttpPost]
        [Route("")]
        public async Task<IActionResult> CreateForPartyProject([FromBody]ProjectSynchronization projectSynchronization)
        {
            var partnerProjectId = projectSynchronization.PartnerId;
            var byPartnerId = _projectSynchronizationMappings.GetByPartnerId<ProjectSynchronization>(partnerProjectId);
            if (byPartnerId != null)
                return StatusCode((int)HttpStatusCode.Conflict, byPartnerId);

            var wrikeAccounts = await _wrikeClient.Accounts.GetAsync().ConfigureAwait(false);
            var wrikeAccount = wrikeAccounts.FirstOrDefault();

            var wrikeAccountRootFolderId = wrikeAccount?.RootFolderId;

            var gitLabProject = _gitLabClient.Projects[int.Parse(partnerProjectId)];
            var newFolder = gitLabProject.ToWrikeFolder();
            var wrikeFolder = await _wrikeClient.FoldersAndProjects.CreateAsync(wrikeAccountRootFolderId, newFolder).ConfigureAwait(false);

            var newProjectSynchronization = new ProjectSynchronization
            {
                Id = Guid.NewGuid().ToString(),
                CreatedAt = DateTime.Now,
                PartnerId = partnerProjectId,
                Name = projectSynchronization.Name ?? wrikeFolder.Title,
                WrikeId = wrikeFolder.Id,
            };

            _projectSynchronizationMappings.Add<ProjectSynchronization>(newProjectSynchronization);
            var mappings = _projectSynchronizationMappings.GetMappings<ProjectSynchronization>();
            _writeToStore.SaveAll(mappings);
            return StatusCode((int)HttpStatusCode.Created, newProjectSynchronization);
        }

        [HttpGet]
        [Route("run")]  
        public async Task<IActionResult> Run([FromQuery]string id)
        {
            try
            {
                var synchronization = _projectSynchronizationMappings.GetMappings<ProjectSynchronization>().FirstOrDefault(x => x.Id == id);
                if (synchronization == null) return NotFound(id);

                var synchronizationProviderTasks = new SynchronizationProvider<TaskSyncable>(_mappings);
                var so = new SynchronizationOrchestrator(_wrikeClient, _gitLabClient, synchronization.PartnerId, synchronization.WrikeId,
                    synchronizationProviderTasks, _writeToStore, _gitLabNotesClient);

                var taskSyncResults = await so.OrchestrateTasks().ConfigureAwait(false);
                var enumerable = taskSyncResults as SynchronizationResult<TaskSyncable>[] ?? taskSyncResults.ToArray();
                var tasksSync = enumerable.Where(x => x.EntityType == SyncEntityType.TaskSyncable).ToArray();
                var wrikeResult = tasksSync.ToPartyResult(PartyType.Wrike);
                var partyResult = tasksSync.ToPartyResult(PartyType.ThirdParty);

                var synchronizationProviderComments = new SynchronizationProvider<CommentSyncable>(synchronizationProviderTasks.Mappings);
                var commentsSyncResults = await so.OrchestrateComments(synchronizationProviderComments).ConfigureAwait(false);
                var enumerable1 = commentsSyncResults as SynchronizationResult<CommentSyncable>[] ?? commentsSyncResults.ToArray();
                var commentsSync = enumerable1.Where(x => x.EntityType == SyncEntityType.Comment).ToArray();
                var wrikeCommentsResult = commentsSync.ToPartyResult(PartyType.Wrike);
                var partyCommentsResult = commentsSync.ToPartyResult(PartyType.ThirdParty);

                UpdateProjectSynchronization(synchronization);
                var runResult = new {WrikeTasksResult = wrikeResult, GitLabTasksResult = partyResult, WrikeCommentsResult = wrikeCommentsResult, GitLabCommentsResult = partyCommentsResult };
                return Ok(runResult);
            }
            catch (Exception e)
            {
                Log.Error(e, e.Message);
                throw;
            }
        }

        private void UpdateProjectSynchronization(ProjectSynchronization synchronization)
        {
            synchronization.UpdatedAt = DateTime.Now;
            _projectSynchronizationMappings.Update<ProjectSynchronization>(synchronization);
            _projectSynchronizationMappings.GetMappings<ProjectSynchronization>();
            _writeToStore.SaveAll(_projectSynchronizationMappings.GetMappings<ProjectSynchronization>());
        }
    }


}