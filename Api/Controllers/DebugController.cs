using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using Zhant.Wrike.Integration.GitLab.Core.Clients;

namespace Zhant.Wrike.Integration.GitLab.Api.Controllers
{
    [Route("api/[controller]")]
    public class DebugController : Controller
    {
        private readonly IGitLabNoteClient _gitLabNoteClient;

        public DebugController(IGitLabNoteClient gitLabNoteClient)
        {
            _gitLabNoteClient = gitLabNoteClient;
        }

        [HttpGet]
        [Route("ping")]
        public async Task<IActionResult> Ping()
        {
           return await Task.Run(() =>
            {
                Log.Debug($"Ping");

                var executingAssembly = Assembly.GetExecutingAssembly();
                var versionAttribute = ((AssemblyInformationalVersionAttribute) executingAssembly
                    .GetCustomAttributes(typeof(AssemblyInformationalVersionAttribute), false).FirstOrDefault())?.InformationalVersion;
                var version = !string.IsNullOrEmpty(versionAttribute)
                    ? versionAttribute
                    : FileVersionInfo.GetVersionInfo(executingAssembly.Location).ProductVersion;
                return Ok($"pong {executingAssembly}, version {version}, location {executingAssembly.Location}.");
            });
        }


        [HttpGet]
        [Route("project/{id:int}/issue/{iid:int}/notes")]
        public async Task<IActionResult> GetForPartyAndWrike(int id, int iid)
        {
            var notes = await _gitLabNoteClient.ForProjectAndIssue(id, iid).ConfigureAwait(false);
            return notes.Any() ? Ok(notes) : (IActionResult)NotFound();
        }
    }
}