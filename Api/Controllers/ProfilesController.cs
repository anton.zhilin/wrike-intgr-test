﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NGitLab;
using Taviloglu.Wrike.ApiClient;
using Taviloglu.Wrike.Core;
using Zhant.Wrike.Integration.GitLab.Api.Models;

namespace Zhant.Wrike.Integration.GitLab.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class ProfilesController : Controller
    {
        private readonly IGitLabClient _gitLabClient;
        private readonly WrikeClient _wrikeClient;

        public ProfilesController(IGitLabClient gitLabClient, WrikeClient wrikeClient)
        {
            _gitLabClient = gitLabClient;
            _wrikeClient = wrikeClient;
        }

        [HttpGet]
        [Route("gitlab/account/info")]
        public async Task<IActionResult> GetGitLabProfileInfo()
        {
            var usersCurrent = _gitLabClient.Users.Current;
            var userInfo = $"{usersCurrent.Name} ({usersCurrent.Username}) e-mail: {usersCurrent.Email}.";
            var projects = await _gitLabClient.Projects.Accessible().ConfigureAwait(false);

            var profileInfo = new PartyProfileInfo {UserInfo = userInfo, UserProjects = projects.Select(x => new PartyProject { Id = x.Id.ToString(), Name = x.Name})};
            return Ok(profileInfo);
        }

        [HttpGet]
        [Route("wrike/user/info")]
        public async Task<IActionResult> GetWrikeProfileInfo()
        {
            var wrikeAccounts = await _wrikeClient.Accounts.GetAsync().ConfigureAwait(false);
            var wrikeAccount = wrikeAccounts.FirstOrDefault();
            var userInfo = $"{wrikeAccount?.Name})";
            var folders = await _wrikeClient.FoldersAndProjects.GetFolderTreeAsync(wrikeAccount?.Id).ConfigureAwait(false);
            var profileInfo = new PartyProfileInfo { UserInfo = userInfo, UserProjects = folders.Where(x => new []{ WrikeTreeScope.WsFolder, WrikeTreeScope.WsRoot}.Contains(x.Scope)).Select(x => new PartyProject {Id= x.Id, Name = x.Title})};
            return Ok(profileInfo);
        }

        /*[HttpGet]
        [Route("project")]
        [NotNull]
        public async Task<IActionResult> SyncronyseProject([FromQuery] int gitLabProjectId, [FromQuery] string synchronizationName)
        {
            var projects = _gitLabClient.Projects;
            Project gitLabProject;
            try
            {
                gitLabProject = projects[gitLabProjectId];
            }
            catch (Exception)
            {
                return NotFound($"Project {gitLabProjectId} not found.");
            }

            var folderTree = await _wrikeClient.GetProjects();
            var rootFolder = folderTree?.Data?.FirstOrDefault(f => f.Scope == Scope.WsRoot);
            if (rootFolder == null)
                return NotFound("Root folder");
            var rootFolderId = rootFolder.Id;

            var projectSynchronization = _synchronizationProvider.CreateProjectSynchronization(rootFolderId, gitLabProjectId, synchronizationName);

            var gitLabProjectName = gitLabProject.Name;
            var targetWrikeFolder = folderTree.Data.FirstOrDefault(d => d.Title == gitLabProjectName);
            var targetWrikeFolderId = targetWrikeFolder?.Id;
            if (targetWrikeFolderId != null)
                gitLabProjectName = $"{gitLabProjectName} - GitLab Import";

            var description =
                $"<br /><br /><br />┆Imported from GitLab <a href=\"{gitLabProject.HttpUrl}\">project</a><br /><br /> on {DateTime.Now:f}";
            var folders = await _wrikeClient.CreateFolder(rootFolderId, gitLabProjectName, description);
            var newFolder = folders.Data.FirstOrDefault();
            if (newFolder == null)
                throw new ApplicationException($"Failed to create {gitLabProjectName} in Wrike.");
            targetWrikeFolderId = newFolder.Id;

            var issues = await _gitLabClient.Issues.ForProject(gitLabProjectId);
            ImportIssuesToWrike(issues, targetWrikeFolderId);

            return Ok($"Project {gitLabProjectName}.");


            return await Task.Run(() =>
            {
                // TODO Configure to trace Debug log. Doesn't work now.
                Log.Debug($"Ping");

                var executingAssembly = Assembly.GetExecutingAssembly();
                var versionAttribute = ((AssemblyInformationalVersionAttribute) executingAssembly
                    .GetCustomAttributes(typeof(AssemblyInformationalVersionAttribute), false).FirstOrDefault())?.InformationalVersion;
                var version = !string.IsNullOrEmpty(versionAttribute)
                    ? versionAttribute
                    : FileVersionInfo.GetVersionInfo(executingAssembly.Location).ProductVersion;
                return Ok($"pong {executingAssembly}, version {version}, location {executingAssembly.Location}.");
            });
        }
  */
        /*
                private void ImportIssuesToWrike(IEnumerable<Issue> issues, string targetWrikeFolderId)
                {
                    foreach (var issue in issues)
                    {
                        var wrikeTask = issue.ToWrikeTask(targetWrikeFolderId);
                        var wrikeResponse = _wrikeClient.CreateTask(wrikeTask).Result;
                        var createdTask = wrikeResponse.Data.First();
                        Log.Information($"Task {createdTask.Id} created. Parent Id {createdTask.ParentIds[0]}.");
                    }
                }
        */
    }
}