using System;
using System.Collections.Generic;
using Zhant.Wrike.Integration.GitLab.Core.Model.Sync;
using Zhant.Wrike.Integration.GitLab.Core.Store;
using Xunit;

namespace Zhant.Wrike.Integration.GitLab.Core.Test
{
    public class SyncronizeItemsFixture
    {
        public SyncronizeItemsFixture()
        {
            // last sync was 3 days ago
            LastSyncDateTime = DateTime.Now.AddDays(-3);
            var synchronizationMaps = new List<SynchronizationMap>
            {
                new SynchronizationMap {/*EntityType = typeof(TaskSyncable).Name, */UpdatedAt = LastSyncDateTime, PartnerId = "1", WrikeId = "10"},
                new SynchronizationMap {/*EntityType = typeof(TaskSyncable).Name, */UpdatedAt = LastSyncDateTime, PartnerId = "2", WrikeId = "20"},
                new SynchronizationMap {/*EntityType = typeof(TaskSyncable).Name, */UpdatedAt = LastSyncDateTime, PartnerId = "3", WrikeId = "30"}
            };
/*            var synchronizationmapsJson = "SynchronizationMaps.json";

            var ws = new WriteStore(synchronizationmapsJson);
            ws.SaveAll(synchronizationMaps);

            var rs = new ReadStore(synchronizationmapsJson);
            var enumerable = rs.GetAll<SynchronizationMap>();*/


            Mappings = new SynchronizationMappings<SynchronizationMap>(synchronizationMaps);
        }

        public SynchronizationMappings<SynchronizationMap> Mappings { get; private set; }
        public DateTime LastSyncDateTime { get; private set; }
    }

    [CollectionDefinition(nameof(SyncronizeItemsFixture))]
    public class CollectionDefinitionPlaceholder : ICollectionFixture<SyncronizeItemsFixture>
    {
        // This class has no code, and is never created. Its purpose is simply
        // to be the place to apply [CollectionDefinition] and all the
        // ICollectionFixture<> interfaces.
    }
}