using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Zhant.Wrike.Integration.GitLab.Core.Model.Sync;
using Xunit;

namespace Zhant.Wrike.Integration.GitLab.Core.Test
{
    [Collection(nameof(SyncronizeItemsFixture))]
    public class When_synchronize_items
    {
        private readonly SynchronizationMappings<SynchronizationMap> _synchronizationMappings;
        private readonly DateTime _lastSyncDateTime;

        public When_synchronize_items(SyncronizeItemsFixture fixture)
        {
            _synchronizationMappings = fixture.Mappings;
            _lastSyncDateTime = fixture.LastSyncDateTime;
        }

        [Fact]
        public void Given_mappings_are_empty_new_items_should_be_created()
        {
            var dateTime = DateTime.Now;
            var sp = new SynchronizationProvider<TaskSyncable>(new SynchronizationMappings<SynchronizationMap>(new List<SynchronizationMap>()));
            var partnerItems = new List<TaskSyncable>
            {
                new TaskSyncable {UpdatedAt = dateTime.AddDays(-2), Id = "10", ParentId = "1"},
                new TaskSyncable {UpdatedAt = dateTime.AddDays(-2), Id = "20", ParentId = "1"}
            };
            var synchronizationAssesment = sp.Synchronize(new List<TaskSyncable>(), partnerItems);
            synchronizationAssesment.WrikeItemsToSync.Count().Should().Be(0);
            synchronizationAssesment.PartnerItemsToSync.Count().Should().Be(2);
        }

        [Fact]
        public void Given_partner_items_was_updated_after_last_sync_and_after_wrike_update_they_should_be_synced()
        {
            var dateTime = DateTime.Now;
            var synchronizationProvider = new SynchronizationProvider<TaskSyncable>(_synchronizationMappings);
            var wrikeItems = new List<TaskSyncable>
            {
                new TaskSyncable {UpdatedAt = dateTime.AddDays(-1), Id = "10", ParentId = "1"},
                new TaskSyncable {UpdatedAt = _lastSyncDateTime, Id = "20", ParentId = "1"},
                new TaskSyncable {UpdatedAt = _lastSyncDateTime, Id = "30", ParentId = "1"}
            };
            var partnerItems = new List<TaskSyncable>
            {
                new TaskSyncable {UpdatedAt = dateTime.AddDays(-2), Id = "1", ParentId = "1"},
                new TaskSyncable {UpdatedAt = dateTime.AddHours(-2), Id = "2", ParentId = "1"},
                new TaskSyncable {UpdatedAt = _lastSyncDateTime, Id = "3", ParentId = "1"}
            };
            var synchronizationAssesment = synchronizationProvider.Synchronize(wrikeItems, partnerItems);

            synchronizationAssesment.PartnerItemsToSync.Count().Should().Be(1);
            synchronizationAssesment.WrikeItemsToSync.Count().Should().Be(1);
            synchronizationAssesment.WrikeItemsToSync.All(x => new[] { "10"}.Contains(x.Entity.Id)).Should().BeTrue();
            synchronizationAssesment.PartnerItemsToSync.All(x => new [] {"2"}.Contains(x.Entity.Id)).Should().BeTrue();
        }

        [Fact]
        public void Given_wrike_items_was_updated_after_last_sync_they_should_be_synced()
        {
            var dateTime = DateTime.Now;
            var synchronizationProvider = new SynchronizationProvider<TaskSyncable>(_synchronizationMappings);
            var wrikeItems = new List<TaskSyncable>
            {
                new TaskSyncable {UpdatedAt = dateTime.AddDays(-1), Id = "10", ParentId = "1"},
                new TaskSyncable {UpdatedAt = _lastSyncDateTime, Id = "20", ParentId = "1"},
                new TaskSyncable {UpdatedAt = _lastSyncDateTime, Id = "30", ParentId = "1"}
            };
            var partnerItems = new List<TaskSyncable>
            {
                new TaskSyncable {UpdatedAt = dateTime.AddDays(-2), Id = "1", ParentId = "1"},
                new TaskSyncable {UpdatedAt = dateTime.AddHours(-2), Id = "2", ParentId = "1"},
                new TaskSyncable {UpdatedAt = _lastSyncDateTime, Id = "3", ParentId = "1"}
            };
            var synchronizationAssesment = synchronizationProvider.Synchronize(wrikeItems, partnerItems);

            synchronizationAssesment.PartnerItemsToSync.Count().Should().Be(1);
            synchronizationAssesment.WrikeItemsToSync.Count().Should().Be(1);
            synchronizationAssesment.WrikeItemsToSync.All(x => new[] { "10" }.Contains(x.Entity.Id)).Should().BeTrue();
            synchronizationAssesment.PartnerItemsToSync.All(x => new[] { "2" }.Contains(x.Entity.Id)).Should().BeTrue();
        }
    }
}