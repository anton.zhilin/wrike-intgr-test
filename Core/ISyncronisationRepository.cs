﻿using System.Collections.Generic;
using Zhant.Wrike.Integration.GitLab.Core.Model.Sync;

namespace Zhant.Wrike.Integration.GitLab.Core
{
    public interface ISynchronizationRepository
    {
        void AddOrUpdateProjectSynchronization(ProjectSynchronization projectSynchronization);
        void AddOrUpdateSynchronizationMap(SynchronizationMap synchronizationMap);
        IEnumerable<ProjectSynchronization> GetProjectSynchronizations();
        IEnumerable<SynchronizationMap> GetSynchronizationMaps();
    }
}