﻿namespace Zhant.Wrike.Integration.GitLab.Core
{
    public enum PartyType
    {
        Unknown = 0,
        Wrike,
        ThirdParty
    }
}