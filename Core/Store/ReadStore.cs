﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using Zhant.Wrike.Integration.GitLab.Core.Model.Sync;

namespace Zhant.Wrike.Integration.GitLab.Core.Store
{
    public interface IReadFromStore
    {
        IEnumerable<T> GetAll<T>() where T : SynchronizationMap, new();
    }

    public class ReadStore : IReadFromStore
    {
        public IEnumerable<T> GetAll<T>() where T : SynchronizationMap, new()
        {
            var folderPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "WrikeTest");
            if (!Directory.Exists(folderPath)) Directory.CreateDirectory(folderPath);
            var fileName = Path.Combine(folderPath, $"{typeof(T).FullName}.json");

            if (!File.Exists(fileName)) return new List<T>();

            var serializer = new JsonSerializer();
            IEnumerable<T> deserialize;
            using (var sw = new StreamReader(fileName))
            {
                using (var writer = new JsonTextReader(sw))
                {
                    deserialize = serializer.Deserialize<IEnumerable<T>>(writer);
                }
            }
            return deserialize;
        }
    }
}