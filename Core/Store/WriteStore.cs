﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using Zhant.Wrike.Integration.GitLab.Core.Model.Sync;

namespace Zhant.Wrike.Integration.GitLab.Core.Store
{
    public interface IWriteToStore
    {
        void SaveAll<T>(IEnumerable<T> items) where T : SynchronizationMap, new();
    }
    public class WriteStore : IWriteToStore
    {
        public void SaveAll<T>(IEnumerable<T> items) where T : SynchronizationMap, new()
        {
            var folderPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "WrikeTest");
            if (!Directory.Exists(folderPath)) Directory.CreateDirectory(folderPath);
            var fileName = Path.Combine(folderPath, $"{typeof(T).FullName}.json");

            var serializer = new JsonSerializer();
            using (var sw = new StreamWriter(fileName, append: false))
            using (var writer = new JsonTextWriter(sw))
            {
                serializer.Serialize(writer, items);
            }
        }
    }
}