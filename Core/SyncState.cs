﻿namespace Zhant.Wrike.Integration.GitLab.Core
{
    public enum SyncState
    {
        Unknown = 0,
        Created,
        Updated,
        Error
    }
}