using System.Net.Http;

namespace Zhant.Wrike.Integration.GitLab.Core.Clients
{
    public abstract class HttpClientBase
    {
        protected HttpClient Client { get; }

        protected HttpClientBase(HttpClient client)
        {
            Client = client;
        }
    }
}
