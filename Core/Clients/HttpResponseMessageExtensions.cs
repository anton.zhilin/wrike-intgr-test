﻿using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Zhant.Wrike.Integration.GitLab.Core.Clients
{
    public static class HttpResponseMessageExtensions
    {
        public static async Task<T> HandleResponse<T>(this HttpResponseMessage response)
        {
            response.EnsureSuccessStatusCode(); // Throws an exception if the status code is lower than HttpStatusCode.OK or higher than 299.
            var content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            return JsonConvert.DeserializeObject<T>(content);
        }
    }
}