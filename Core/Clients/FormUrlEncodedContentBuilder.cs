using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Http;
using Newtonsoft.Json;

namespace Zhant.Wrike.Integration.GitLab.Core.Clients
{
    public class FormUrlEncodedContentBuilder
    {
        private readonly List<KeyValuePair<string, string>> _pairs;

        public FormUrlEncodedContentBuilder()
        {
            _pairs = new List<KeyValuePair<string, string>>();
        }

        public FormUrlEncodedContentBuilder AddParameter(string key, object value)
        {
            switch (value)
            {
                case IList list:
                    if (list.Count > 0) _pairs.Add(new KeyValuePair<string, string>(key, JsonConvert.SerializeObject(list)));
                    break;
                case string text:
                    _pairs.Add(new KeyValuePair<string, string>(key, text));
                    break;
                default:
                    throw new ArgumentException($"{value.GetType()} is not implemented");
            }

            return this;
        }

        public FormUrlEncodedContent Build()
        {
            return new FormUrlEncodedContent(_pairs);
        }
    }
}