﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using NGitLab.Models;

namespace Zhant.Wrike.Integration.GitLab.Core.Clients
{
    public interface IGitLabNoteClient
    {
        /// <summary>
        /// Get list of Note for the selected project and issue id.
        /// </summary>
        Task<IEnumerable<Note>> ForProjectAndIssue(int projectId, int issueId);

        /// <summary>Return a single issue for a project given project.</summary>
        Task<Note> GetAsync(int projectId, int issueId, int noteId);

        /// <summary>
        /// Add an Note with he proposed title to the GitLab list for the selected project and issue id.
        /// </summary>
        Task<Note> CreateAsync(int projectId, long issueId, string noteBody);

    }

    /// <summary>
    /// Specialized client for GitLab Notes API, as it not implemented in NGitLab.
    /// </summary>
    public class GitLabNoteClient : HttpClientBase, IGitLabNoteClient
    {

        public GitLabNoteClient(HttpClient client) : base(client)
        {
        }

        public async Task<IEnumerable<Note>> ForProjectAndIssue(int projectId, int issueId)
        {
            var response = await Client.GetAsync($"projects/{projectId}/issues/{issueId}/notes").ConfigureAwait(false);
            if (response.StatusCode == HttpStatusCode.NotFound)
                return new List<Note>();
            return await response.HandleResponse<IEnumerable<Note>>().ConfigureAwait(false);
        }

        public async Task<Note> GetAsync(int projectId, int issueId, int noteId)
        {
            var response = await Client.GetAsync($"projects/{projectId}/issues/{issueId}/notes/{noteId}").ConfigureAwait(false);
            if (response.StatusCode == HttpStatusCode.NotFound)
                return new Note();
            return await response.HandleResponse<Note>().ConfigureAwait(false);
        }

        public async Task<Note> CreateAsync(int projectId, long issueId, string noteBody)
        {
            var requestUri = $"projects/{projectId}/issues/{issueId}/notes?body={noteBody}";
            var postAsJsonAsync = await Client.PostAsync(requestUri, null).ConfigureAwait(false);
            return await postAsJsonAsync.Content.ReadAsAsync<Note>();
        }
    }
}
