using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Wrike.Integration.GitLab.Core.Model.Wrike;

namespace Wrike.Integration.GitLab.Core.Clients
{
    public interface IMyWrikeClient
    {
        Task<WrikeResponse<WrikeFolder>> GetProjects();
        Task<WrikeResponse<Folder>> CreateFolder(string parentFolderId, string title, string description = null);
        Task<WrikeResponse<WrikeTask>> CreateTask(WrikeTask task);
        Task<WrikeResponse<WrikeTask>> GetFolderTasks(string folderId);
    }

    public class MyWrikeClient : HttpClientBase, IMyWrikeClient
    {
        public MyWrikeClient(HttpClient client) : base(client)
        {
        }

        public async Task<WrikeResponse<WrikeFolder>> GetProjects()
        {
            var response = await Client.GetAsync("folders").ConfigureAwait(false);
            if (response.StatusCode == HttpStatusCode.NotFound)
                return null;
            return await response.HandleResponse<WrikeResponse<WrikeFolder>>().ConfigureAwait(false);
        }
        public async Task<WrikeResponse<WrikeTask>> GetFolderTasks(string folderId)
        {
            var response = await Client.GetAsync($"folders/{folderId}/comments").ConfigureAwait(false);
            if (response.StatusCode == HttpStatusCode.NotFound)
                return null;
            return await response.HandleResponse<WrikeResponse<WrikeTask>>().ConfigureAwait(false);
        }

        public async Task<WrikeResponse<Folder>> CreateFolder(string parentFolderId, string title, string description = null)
        {
            var content = new FormUrlEncodedContentBuilder().AddParameter("title", title).AddParameter("description", description).Build();
            var responseMessage = await Client.PostAsync($"folders/{parentFolderId}/folders", content).ConfigureAwait(false);
            return await responseMessage.Content.ReadAsAsync<WrikeResponse<Folder>>();
        }

        public async Task<WrikeResponse<WrikeTask>> CreateTask(WrikeTask task)
        {
            var parentFolderId = task.ParentIds.FirstOrDefault();
            var content = new FormUrlEncodedContentBuilder()
                .AddParameter("title", task.Title)
                .AddParameter("description", task.Description)
                .AddParameter("status", task.Status.ToString())
                .AddParameter("parents", task.ParentIds)
                .Build();
            var responseMessage = await Client.PostAsync($"folders/{parentFolderId}/tasks", content).ConfigureAwait(false);
            return await responseMessage.Content.ReadAsAsync<WrikeResponse<WrikeTask>>();
        }
    }
}
