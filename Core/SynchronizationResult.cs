﻿using System;
using System.Linq;
using Zhant.Wrike.Integration.GitLab.Core.Model.Sync;

namespace Zhant.Wrike.Integration.GitLab.Core
{
    public class SynchronizationResult<TSyncable> where TSyncable : EntityBase
    {
        public SynchronizationResult(PartyType party, SyncState syncState, Syncable<TSyncable> syncable)
        {
            Party = party;
            EntityType = (SyncEntityType) Enum.Parse(typeof(SyncEntityType), typeof(TSyncable).Name);
            SyncState = syncState;
            Syncable = syncable;
        }

        /// <summary>
        /// Party, where updated happened.
        /// </summary>
        public PartyType Party { get; }
        public SyncEntityType EntityType { get; }
        public SyncState SyncState { get; }
        public Syncable<TSyncable> Syncable { get; }
        public DateTime UpdatedAt { get; set; }
        public string Message { get; set; }
    }

    public class PartyResult
    {
        public string Party { get; set; }
        public int Created { get; set; }
        public int Updated { get; set; }
        public int Error { get; set; }
        public string Messages { get; set; }
    }

    public static class Extension
    {
        public static PartyResult ToPartyResult<T>(this SynchronizationResult<T>[] tasksSync, PartyType partyType) where T : EntityBase
        {
            if (!tasksSync.Any()) return new PartyResult {Party = partyType.ToString()};
            var results = tasksSync.Where(x => x.Party == partyType).ToArray();
            var taskCreated = results.Count(x => x.SyncState == SyncState.Created);
            var taskUpdated = results.Count(x => x.SyncState == SyncState.Updated);
            var taskError = results.Count(x => x.SyncState == SyncState.Error);
            var errorMessages = string.Join(Environment.NewLine, results.Select(x => x.Message).ToArray());// results.Select(x => x.Message).Aggregate((a, b) => a + Environment.NewLine + b);
            var partyResult = new PartyResult { Party = partyType.ToString(), Created = taskCreated, Updated = taskUpdated, Error = taskError, Messages = errorMessages };
            return partyResult;
        }
    }
}