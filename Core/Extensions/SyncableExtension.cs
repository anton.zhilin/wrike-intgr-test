﻿using System;
using System.Linq;
using NGitLab.Models;
using Taviloglu.Wrike.Core;
using Zhant.Wrike.Integration.GitLab.Core.Model.Sync;

namespace Zhant.Wrike.Integration.GitLab.Core.Extensions
{
    public static class SyncableExtension
    {
        public static WrikeFolder ToWrikeFolder(this Project gitLabProject)
        {
            var title = gitLabProject.Name;
            var description = gitLabProject.Description;
            var newFolder = new WrikeFolder { Title = title, Description = description };
            return newFolder;
        }

        public static TaskSyncable ToTaskSyncable(this Issue issue)
        {
            return new TaskSyncable
            {
                Id = issue.Id.ToString(),
                ParentId = issue.ProjectId.ToString(),
                CreatedAt = issue.CreatedAt,
                UpdatedAt = issue.UpdatedAt,
                Status = issue.State,
                Title = issue.Title,
                Description = issue.Description,
                //TODO if user exist both parties can be mapped by e-mail
                //                    Author = new UserSyncable {Email = x.Author.Email, Name = x.Author.Name},
                //                    Assignee = new UserSyncable {Email = x.Assignee.Email, Name = x.Assignee.Name}
            };
        }

        public static TaskSyncable ToTaskSyncable(this WrikeTask wrikeTask)
        {
            return new TaskSyncable
            {
                Id = wrikeTask.Id.ToString(),
                ParentId = wrikeTask.ParentIds?.FirstOrDefault(),
                CreatedAt = wrikeTask.CreatedDate,
                UpdatedAt = wrikeTask.UpdatedDate,
                Status = wrikeTask.Status.ToString(),
                Title = wrikeTask.Title,
                Description = wrikeTask.Description
                //TODO if user exist both parties can be mapped by e-mail
                //                    Author = new UserSyncable {Email = x.Author.Email, Name = x.Author.Name},
                //                    Assignee = new UserSyncable {Email = x.Assignee.Email, Name = x.Assignee.Name}
            }; ;
        }

        public static CommentSyncable ToCommentSyncable(this WrikeComment comment)
        {
            return new CommentSyncable
            {
                Id = comment.Id,
                CreatedAt = comment.CreatedDate,
                Text = comment.Text,
                ParentId = comment.TaskId,
                // Wrike comment does not have UpdatedAt property.
                UpdatedAt = default(DateTime?)
            };
        }

        public static CommentSyncable ToCommentSyncable(this Note comment)
        {
            return new CommentSyncable
            {
                Id = comment.Id.ToString(),
                CreatedAt = comment.CreatedAt,
                Text = comment.Body,
                ParentId = comment.NoteableId.ToString(),
                // forcible set to default as Wrike comment does not have UpdatedAt property.
                UpdatedAt = default(DateTime?)
            };
        }

        public static WrikeTask ToWrikeTask(this Syncable<TaskSyncable> taskSyncable)
        {
            var task = taskSyncable.Entity;
            return new WrikeTask
            {
                Id = taskSyncable.OtherPartyId,
                Title = task.Title,
                Description = task.Description,
                Status = task.Status.ToLowerInvariant() == "closed" ? WrikeTaskStatus.Completed : WrikeTaskStatus.Active,
            };
        }

        public static WrikeComment ToWrikeComment(this Syncable<CommentSyncable> syncable)
        {
            var commentSyncable = syncable.Entity;
            return new WrikeComment
            {
                Id = syncable.OtherPartyId,
                Text = commentSyncable.Text
            };
        }

        public static Note ToNote(this Syncable<CommentSyncable> syncable)
        {
            var commentSyncable = syncable.Entity;  
            return new Note
            {
                Id = long.Parse(syncable.OtherPartyId),
                Body = commentSyncable.Text
            };
        }

        public static IssueEdit ToGitLabIssue(this Syncable<TaskSyncable> taskSyncable)
        {
            var task = taskSyncable.Entity;
            string state;
            var value = task.Status.ToLowerInvariant();
            switch (value)
            {
                case "active":
                case "deferred":
                    state = "opened";
                    break;
                case "cancelled":
                case "closed":
                case "completed":
                    state = "closed";
                    break;
                default:
                    throw new ArgumentOutOfRangeException(value);
            }

            return new IssueEdit
            {
                Id = int.Parse(taskSyncable.OtherPartyId),
                Title = task.Title,
                Description = task.Description,
                State = state,
            };
        }
    }
}