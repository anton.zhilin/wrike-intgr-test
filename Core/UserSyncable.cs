﻿namespace Zhant.Wrike.Integration.GitLab.Core
{
    public class UserSyncable
    {
        public string Email { get; set; }
        public string Name { get; set; }
    }
}