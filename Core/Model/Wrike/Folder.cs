﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Wrike.Integration.GitLab.Core.Model.Wrike
{
    public class Folder
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("accountId")]
        public string AccountId { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("createdDate")]
        public DateTime CreatedDate { get; set; }

        [JsonProperty("updatedDate")]
        public DateTime UpdatedDate { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("sharedIds")]
        public List<string> SharedIds { get; set; }

        [JsonProperty("parentIds")]
        public List<string> ParentIds { get; set; }

        [JsonProperty("childIds")]
        public List<object> ChildIds { get; set; }

        [JsonProperty("superParentIds")]
        public List<object> SuperParentIds { get; set; }

        [JsonProperty("scope")]
        public string Scope { get; set; }

        [JsonProperty("hasAttachments")]
        public bool HasAttachments { get; set; }

        [JsonProperty("permalink")]
        public string Permalink { get; set; }

        [JsonProperty("workflowId")]
        public string WorkflowId { get; set; }

        [JsonProperty("metadata")]
        public List<object> Metadata { get; set; }

        [JsonProperty("customFields")]
        public List<object> CustomFields { get; set; }
    }
}