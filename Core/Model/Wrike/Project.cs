﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Wrike.Integration.GitLab.Core.Model.Wrike
{
    public class Project
    {
        [JsonProperty("authorId")]
        public string AuthorId { get; set; }

        [JsonProperty("ownerIds")]
        public List<string> OwnerIds { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("startDate")]
        public DateTime StartDate { get; set; }

        [JsonProperty("endDate")]
        public DateTime EndDate { get; set; }

        [JsonProperty("createdDate")]
        public DateTime CreatedDate { get; set; }
    }
}