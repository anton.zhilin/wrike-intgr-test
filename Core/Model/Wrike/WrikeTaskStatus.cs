﻿namespace Wrike.Integration.GitLab.Core.Model.Wrike
{
    public enum WrikeTaskStatus
    {
        Active,
        Completed,
        Deferred,
        Cancelled
    }
}