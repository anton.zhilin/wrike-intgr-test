﻿using System;
using Newtonsoft.Json;

namespace Wrike.Integration.GitLab.Core.Model.Wrike
{
    static class ScopeExtensions
    {
        public static Scope? ValueForString(string str)
        {
            switch (str)
            {
                case "RbFolder": return Scope.RbFolder;
                case "RbRoot": return Scope.RbRoot;
                case "WsFolder": return Scope.WsFolder;
                case "WsRoot": return Scope.WsRoot;
                default: return null;
            }
        }

        public static Scope ReadJson(JsonReader reader, JsonSerializer serializer)
        {
            var str = serializer.Deserialize<string>(reader);
            var maybeValue = ValueForString(str);
            if (maybeValue.HasValue) return maybeValue.Value;
            throw new Exception("Unknown enum case " + str);
        }

        public static void WriteJson(this Scope value, JsonWriter writer, JsonSerializer serializer)
        {
            switch (value)
            {
                case Scope.RbFolder: serializer.Serialize(writer, "RbFolder"); break;
                case Scope.RbRoot: serializer.Serialize(writer, "RbRoot"); break;
                case Scope.WsFolder: serializer.Serialize(writer, "WsFolder"); break;
                case Scope.WsRoot: serializer.Serialize(writer, "WsRoot"); break;
            }
        }
    }
}