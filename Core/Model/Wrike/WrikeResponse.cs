﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Wrike.Integration.GitLab.Core.Model.Wrike
{
    public class WrikeResponse<T>
    {
        [JsonProperty("kind")]
        public string Kind { get; set; }

        [JsonProperty("data")]
        public List<T> Data { get; set; }
    }
}