﻿namespace Wrike.Integration.GitLab.Core.Model.Wrike
{
    public enum Scope { RbFolder, RbRoot, WsFolder, WsRoot }
}