﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Wrike.Integration.GitLab.Core.Model.Wrike
{
    public class WrikeFolder
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("childIds")]
        public List<string> ChildIds { get; set; }

        [JsonProperty("scope")]
        public Scope Scope { get; set; }

        [JsonProperty("project")]
        public Project Project { get; set; }
    }
}