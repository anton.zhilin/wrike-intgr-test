﻿using System;
using Newtonsoft.Json;

namespace Wrike.Integration.GitLab.Core.Model.Wrike
{
    public sealed class WrikeTaskDate
    {
        [JsonProperty("start")]
        [JsonConverter(typeof(CustomDateTimeConverter), "yyyy-MM-dd'T'HH:mm:ss")]
        public DateTime Start { get; set; }

        [JsonProperty("due")]
        [JsonConverter(typeof(CustomDateTimeConverter), "yyyy-MM-dd'T'HH:mm:ss")]
        public DateTime Due { get; set; }
    }
}