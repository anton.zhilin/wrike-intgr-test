﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using NGitLab.Models;
using Zhant.Wrike.Integration.GitLab.Core.Clients;

// ReSharper disable once CheckNamespace
namespace NGitLab.Models
{
    public partial class Note
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("body")]
        public string Body { get; set; }

        [JsonProperty("attachment")]
        public object Attachment { get; set; }

        [JsonProperty("author")]
        public Author Author { get; set; }

        [JsonProperty("created_at")]
        public DateTime CreatedAt { get; set; }

        [JsonProperty("updated_at")]
        public DateTime UpdatedAt { get; set; }

        [JsonProperty("system")]
        public bool System { get; set; }

        [JsonProperty("noteable_id")]
        public long NoteableId { get; set; }

        [JsonProperty("noteable_type")]
        public string NoteableType { get; set; }

        [JsonProperty("noteable_iid")]
        public long NoteableIid { get; set; }
    }
}
