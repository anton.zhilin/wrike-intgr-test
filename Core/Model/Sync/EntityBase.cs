﻿using System;

namespace Zhant.Wrike.Integration.GitLab.Core.Model.Sync
{
    public abstract class EntityBase
    {
        public string Id { get; set; }
        public string ParentId { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
    }
}