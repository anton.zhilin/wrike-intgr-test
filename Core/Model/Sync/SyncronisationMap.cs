﻿using System;

namespace Zhant.Wrike.Integration.GitLab.Core.Model.Sync
{
    public class SynchronizationMap
    {
        public SynchronizationMap()
        {
            EntityType = GetType().Name;
            CreatedAt = DateTime.Now;
        }

        public string EntityType { get; }
        public string WrikeId { get; set; }
        public string PartnerId { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}