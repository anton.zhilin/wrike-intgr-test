﻿namespace Zhant.Wrike.Integration.GitLab.Core.Model.Sync
{
    public class CommentSyncable : EntityBase
    {
        public string Text { get; set; }
    }
}