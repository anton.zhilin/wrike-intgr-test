﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Zhant.Wrike.Integration.GitLab.Core.Model.Sync
{
    public interface ISynchronizationMappings<TMap> where TMap : SynchronizationMap, new()
    {
        TMap GetByWrikeId<TSyncable>(string wrikeId);
        TMap GetByPartnerId<TSyncable>(string partnerId);
        void Add<TSyncable>(TMap map);
        void Update<TSyncable>(TMap map);
        IEnumerable<TMap> GetMappings<TSyncable>();
    }

    public class SynchronizationMappings<TMap> : ISynchronizationMappings<TMap> where TMap : SynchronizationMap, new() 
    {
        private readonly List<TMap> _mappings;

        public SynchronizationMappings(IEnumerable<TMap> mappings)
        {
            _mappings = mappings?.ToList() ?? new List<TMap>();
        } 

        public TMap GetByWrikeId<TSyncable>(string wrikeId)
        {
            return _mappings.FirstOrDefault(s => s.WrikeId == wrikeId && s.EntityType == typeof(TSyncable).Name);
        }

        public TMap GetByPartnerId<TSyncable>(string partnerId)
        {
            return _mappings.FirstOrDefault(s => s.PartnerId == partnerId && s.EntityType == typeof(TSyncable).Name);
        }

        public void Add<TSyncable>(TMap map)
        {
            _mappings.Add(map);
        }
        
        public void Update<TSyncable>(TMap map)
        {
            var firstOrDefault = _mappings.FirstOrDefault(x => x.PartnerId == map.PartnerId && x.WrikeId == map.WrikeId && x.EntityType == map.EntityType);
            if (firstOrDefault == null) 
                throw new ApplicationException($"Update of {map.EntityType} failed. Map not found: wrikeId {map.WrikeId}, partnerId {map.PartnerId}.");
            firstOrDefault.UpdatedAt = map.UpdatedAt;
        }

        public IEnumerable<TMap> GetMappings<TSyncable>()
        {
            return _mappings.Where(m => m.EntityType == typeof(TSyncable).Name);
        }
    }
}