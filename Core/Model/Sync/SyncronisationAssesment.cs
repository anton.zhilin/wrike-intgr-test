﻿using System.Collections.Generic;

namespace Zhant.Wrike.Integration.GitLab.Core.Model.Sync
{
    public class SynchronizationAssessment<T> where T : EntityBase
    {
        /// <summary>
        /// Wrike items created/changed after the last sync
        /// </summary>
        public IEnumerable<Syncable<T>> WrikeItemsToSync { get; set; }
        /// <summary>
        /// Partner items created/changed after the last sync
        /// </summary>
        public IEnumerable<Syncable<T>> PartnerItemsToSync { get; set; }
    }
}