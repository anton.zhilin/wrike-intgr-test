﻿namespace Zhant.Wrike.Integration.GitLab.Core.Model.Sync
{
    public class ProjectSynchronization : SynchronizationMap
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }

}