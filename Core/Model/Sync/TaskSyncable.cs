﻿namespace Zhant.Wrike.Integration.GitLab.Core.Model.Sync
{
    public class TaskSyncable : EntityBase
    {
        public string Status { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public UserSyncable Author { get; set; }
        public UserSyncable Assignee { get; set; }
    }
}