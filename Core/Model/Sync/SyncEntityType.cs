﻿namespace Zhant.Wrike.Integration.GitLab.Core.Model.Sync
{
    public enum SyncEntityType
    {
        Undefined = 0,
        Project,
        TaskSyncable,
        Comment
    }
}