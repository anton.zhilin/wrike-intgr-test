﻿namespace Zhant.Wrike.Integration.GitLab.Core.Model.Sync
{
    public class Syncable<T> where T : EntityBase
    {
        public T Entity { get; set; }
        public string OtherPartyId { get; set; }
    }
}