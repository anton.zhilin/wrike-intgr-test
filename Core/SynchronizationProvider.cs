﻿using System;
using System.Collections.Generic;
using System.Linq;
using Zhant.Wrike.Integration.GitLab.Core.Model.Sync;

namespace Zhant.Wrike.Integration.GitLab.Core
{
    public class SynchronizationProvider<T> where T : EntityBase
    {
        public ISynchronizationMappings<SynchronizationMap> Mappings { get; }

        public SynchronizationProvider(ISynchronizationMappings<SynchronizationMap> synchronizationMappings)
        {
            Mappings = synchronizationMappings ?? throw new ArgumentNullException(nameof(synchronizationMappings));
        }

        public SynchronizationAssessment<T> Synchronize(IEnumerable<T> wrikeItems, IEnumerable<T> partnerItems)
        {
            var wrikeItemList = wrikeItems.ToList();
            var partnerItemList = partnerItems.ToList();

            // 1. WrikeItems to sync
            var wrikeItemsChangedAfterLastSync = wrikeItemList.Any()
                ? wrikeItemList
                    .Select(item => new
                    {
                        Item = item,
                        Map = Mappings.GetByWrikeId<T>(item.Id)
                    })
                    .Select(mappedItem => new {mappedItem, Party = partnerItemList.FirstOrDefault(x => x.Id == mappedItem.Map?.PartnerId)})
                    .Where(itemMappedToParty => itemMappedToParty.mappedItem.Map == null ||
                                                itemMappedToParty.mappedItem.Item.UpdatedAt > itemMappedToParty.mappedItem.Map?.UpdatedAt
                                                && itemMappedToParty.mappedItem.Item.UpdatedAt > itemMappedToParty.Party?.UpdatedAt)
                    .Select(itemMap => new Syncable<T> {Entity = itemMap.mappedItem.Item, OtherPartyId = itemMap.mappedItem.Map?.PartnerId})
                : new List<Syncable<T>>()
                    .ToList();

            // 2. partnerItems to sync
            var partnerItemsChangedAfterLastSync = partnerItemList.Any()
                ? partnerItemList
                    .Select(item => new
                    {
                        Item = item,
                        Map = Mappings.GetByPartnerId<T>(item.Id)
                    })
                    .Select(mappedItem => new {mappedItem, Party = wrikeItemList.FirstOrDefault(x => x.Id == mappedItem.Map?.WrikeId)})
                    .Where(itemMappedToParty => itemMappedToParty.mappedItem.Map == null ||
                                                itemMappedToParty.mappedItem.Item.UpdatedAt > itemMappedToParty.mappedItem.Map.UpdatedAt
                                                && itemMappedToParty.mappedItem.Item.UpdatedAt > itemMappedToParty.Party?.UpdatedAt)
                    .Select(itemMap => new Syncable<T> {Entity = itemMap.mappedItem.Item, OtherPartyId = itemMap.mappedItem.Map?.WrikeId})
                : new List<Syncable<T>>()
                    .ToList();


            var assessment = new SynchronizationAssessment<T>
            {
                WrikeItemsToSync = wrikeItemsChangedAfterLastSync,
                PartnerItemsToSync = partnerItemsChangedAfterLastSync
            };
            return assessment;
        }

        public void UpdateMappings<TSyncable>(IEnumerable<SynchronizationResult<TSyncable>> synchronizationResults) where TSyncable : EntityBase
        {
            var succeededSyncs = synchronizationResults.Where(x => !new[] { SyncState.Error, SyncState.Unknown }.Contains(x.SyncState));
            foreach (var synced in succeededSyncs)
            {
                var wrikeId = synced.Party == PartyType.Wrike ? synced.Syncable.OtherPartyId : synced.Syncable.Entity.Id;
                var partyId = synced.Party == PartyType.ThirdParty ? synced.Syncable.OtherPartyId : synced.Syncable.Entity.Id;
                switch (synced.SyncState)
                {
                    case SyncState.Updated:
                        Mappings.Update<SynchronizationMap>(new SynchronizationMap
                        {
                            PartnerId = partyId,
                            UpdatedAt = synced.UpdatedAt,
                            WrikeId = wrikeId,
//                            EntityType = synced.EntityType.ToString()
                        });
                        break;

                    case SyncState.Created:
                        Mappings.Add<SynchronizationMap>(new SynchronizationMap
                        {
                            PartnerId = partyId,
                            CreatedAt = synced.UpdatedAt,
                            UpdatedAt = synced.UpdatedAt,
                            WrikeId = wrikeId,
//                            EntityType = synced.EntityType.ToString()
                        });
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

    }
}