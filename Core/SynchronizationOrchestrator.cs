﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NGitLab;
using NGitLab.Models;
using Serilog;
using Taviloglu.Wrike.ApiClient;
using Taviloglu.Wrike.Core;
using Zhant.Wrike.Integration.GitLab.Core.Clients;
using Zhant.Wrike.Integration.GitLab.Core.Extensions;
using Zhant.Wrike.Integration.GitLab.Core.Model.Sync;
using Zhant.Wrike.Integration.GitLab.Core.Store;

namespace Zhant.Wrike.Integration.GitLab.Core
{
    public class SynchronizationOrchestrator
    {
        private readonly WrikeClient _wrikeClient;
        private readonly IGitLabClient _gitLabClient;
        private readonly IGitLabNoteClient _gitLabNotesClient;
        private readonly IWriteToStore _writeToStore;
        private readonly string _partnerProjectId;
        private readonly string _wrikeProjectFolderId;
        private readonly SynchronizationProvider<TaskSyncable> _synchronizationProviderTasks;

        public SynchronizationOrchestrator(WrikeClient wrikeClient, IGitLabClient gitLabClient,
            string partnerProjectId, string wrikeProjectFolderId, SynchronizationProvider<TaskSyncable> synchronizationProviderTasks, IWriteToStore writeToStore, IGitLabNoteClient gitLabNotesClient)
        {
            _wrikeClient = wrikeClient;
            _gitLabClient = gitLabClient;
            _partnerProjectId = partnerProjectId ?? throw new ArgumentNullException(nameof(partnerProjectId));
            _wrikeProjectFolderId = wrikeProjectFolderId;
            _synchronizationProviderTasks = synchronizationProviderTasks;
            _writeToStore = writeToStore;
            _gitLabNotesClient = gitLabNotesClient;
        }

        public async Task<IEnumerable<SynchronizationResult<TaskSyncable>>> OrchestrateTasks()
        {
            return await SynchonizeTasks(_wrikeProjectFolderId, _partnerProjectId, _synchronizationProviderTasks).ConfigureAwait(false);
        }

        public async Task<IEnumerable<SynchronizationResult<CommentSyncable>>> OrchestrateComments(SynchronizationProvider<CommentSyncable> synchronizationProvider)
        {
           return await SynchonizeComments(_partnerProjectId, synchronizationProvider).ConfigureAwait(false);
        }

        private async Task<IEnumerable<SynchronizationResult<CommentSyncable>>> SynchonizeComments(string partnerProjectId, SynchronizationProvider<CommentSyncable> synchronizationProvider)
        {
            // TODO
            var allWrikeTaskIds = _synchronizationProviderTasks.Mappings.GetMappings<SynchronizationMap>().Select(x => x.WrikeId).ToArray();
            var wrikeComments = new List<WrikeComment>();
            foreach (var wrikeTaskId in allWrikeTaskIds)
            {
                var comments = await _wrikeClient.Comments.GetAsync(taskId: wrikeTaskId).ConfigureAwait(false);
                wrikeComments.AddRange(comments);
            }

            var wrikeSyncables = wrikeComments.Select(x => x.ToCommentSyncable());

            var allPartnerTaskId = _synchronizationProviderTasks.Mappings.GetMappings<SynchronizationMap>().Select(x => x.PartnerId).ToArray();
            var partnerComments = new List<Note>();
            foreach (var taskId in allPartnerTaskId)
            {
                var comments = await _gitLabNotesClient.ForProjectAndIssue(int.Parse(partnerProjectId), int.Parse(taskId)).ConfigureAwait(false);
                var collection = comments.Where(x => !x.System && x.NoteableType == "Issue");
                partnerComments.AddRange(collection);
            }

            var partnerSyncables = partnerComments.Select(x => x.ToCommentSyncable());
            var assessment = synchronizationProvider.Synchronize(wrikeSyncables, partnerSyncables);
            var partnerSynchronizationResult = await CreateOrUpdatePartnerCommentsOnWrikeAsync(assessment.PartnerItemsToSync);
            var wrikeSynchronizationResult = await UpdateWrikeTasksOnPartner(assessment.WrikeItemsToSync.Where(x => !string.IsNullOrEmpty(x.OtherPartyId)));
            var synchronizationResults = partnerSynchronizationResult.Concat(wrikeSynchronizationResult).ToArray();
            synchronizationProvider.UpdateMappings(synchronizationResults);
            _writeToStore.SaveAll(synchronizationProvider.Mappings.GetMappings<SynchronizationMap>());
            return synchronizationResults;
        }

        private async Task<IEnumerable<SynchronizationResult<TaskSyncable>>> SynchonizeTasks(string wrikeFolderId, string partnerProjectId, SynchronizationProvider<TaskSyncable> synchronizationProvider)
        {
            var glIssues = await _gitLabClient.Issues.ForProject(int.Parse(partnerProjectId)).ConfigureAwait(false);
            var wrTasks = await _wrikeClient.Tasks.GetAsync(folderId: wrikeFolderId).ConfigureAwait(false);

            var partnerSyncables = glIssues.Select(x => x.ToTaskSyncable());
            var wrikeSyncables = wrTasks.Select(x => x.ToTaskSyncable());

            var assessment = synchronizationProvider.Synchronize(wrikeSyncables, partnerSyncables);
            var partnerTasksSynchronizationResult = await CreateOrUpdatePartnerTasksOnWrike(assessment.PartnerItemsToSync, wrikeFolderId);
            var wrikeTasksSynchronizationResult = await UpdateWrikeTasksOnPartner(assessment.WrikeItemsToSync.Where(x => !string.IsNullOrEmpty(x.OtherPartyId)));
            var synchronizationResults = partnerTasksSynchronizationResult.Concat(wrikeTasksSynchronizationResult).ToArray();
            synchronizationProvider.UpdateMappings(synchronizationResults);
            _writeToStore.SaveAll(synchronizationProvider.Mappings.GetMappings<SynchronizationMap>());
            return synchronizationResults;
        }

        /// <summary>
        /// We update from Wrike to 3rd party only tasks imported before from the latter.
        /// </summary>
        private async Task<IEnumerable<SynchronizationResult<TaskSyncable>>> UpdateWrikeTasksOnPartner(IEnumerable<Syncable<TaskSyncable>> syncables)
        {
            var synchronizationResults = new List<SynchronizationResult<TaskSyncable>>();
            foreach (var syncable in syncables.Where(x => !string.IsNullOrEmpty(x.OtherPartyId)))
                try
                {
                    var partnerTask = syncable.ToGitLabIssue();
                    var issue = await _gitLabClient.Issues.EditAsync(partnerTask);
                    synchronizationResults.Add(new SynchronizationResult<TaskSyncable>(PartyType.ThirdParty, SyncState.Updated, syncable){UpdatedAt = issue.UpdatedAt});
                }
                catch (Exception e)
                {
                    Log.Error(e, $"UpdateWrikeTasksOnPartner Id {syncable.OtherPartyId}, WrikeId {syncable.Entity.Id}. Error: {e.Message}.");
                    synchronizationResults.Add(new SynchronizationResult<TaskSyncable>(PartyType.ThirdParty, SyncState.Error, syncable) { Message = e.Message});
                }

            return synchronizationResults.AsEnumerable();
        }

        private async Task<IEnumerable<SynchronizationResult<CommentSyncable>>> UpdateWrikeTasksOnPartner(IEnumerable<Syncable<CommentSyncable>> syncables)
        {
            var synchronizationResults = new List<SynchronizationResult<CommentSyncable>>();
            foreach (var syncable in syncables.Where(x => !string.IsNullOrEmpty(x.OtherPartyId)))
                try
                {
                    var note = syncable.ToNote();
                    var issue = await _gitLabNotesClient.CreateAsync(int.Parse(_partnerProjectId), note.NoteableId, note.Body);
                    synchronizationResults.Add(new SynchronizationResult<CommentSyncable>(PartyType.ThirdParty, SyncState.Updated, syncable) { UpdatedAt = issue.UpdatedAt });
                }
                catch (Exception e)
                {
                    Log.Error(e, $"UpdateWrikeTasksOnPartner Id {syncable.OtherPartyId}, WrikeId {syncable.Entity.Id}. Error: {e.Message}.");
                    synchronizationResults.Add(new SynchronizationResult<CommentSyncable>(PartyType.ThirdParty, SyncState.Error, syncable) { Message = e.Message });
                }

            return synchronizationResults.AsEnumerable();
        }

        private async Task<IEnumerable<SynchronizationResult<TaskSyncable>>> CreateOrUpdatePartnerTasksOnWrike(IEnumerable<Syncable<TaskSyncable>> syncables, string wrikeProjectFolderId)
        {
            var synchronizationResults = new List<SynchronizationResult<TaskSyncable>>();
            foreach (var syncable in syncables)
            {
                try
                {
                    var wrikeTask = syncable.ToWrikeTask();
                    if (string.IsNullOrEmpty(wrikeTask.Id))
                    {
                        var task = await _wrikeClient.Tasks.CreateAsync(wrikeProjectFolderId, wrikeTask);
                        syncable.OtherPartyId = task.Id;
                        synchronizationResults.Add(new SynchronizationResult<TaskSyncable>(PartyType.Wrike, SyncState.Created, syncable) { UpdatedAt = task.UpdatedDate });
                    }
                    else
                    {
                        var task = await _wrikeClient.Tasks.UpdateAsync(taskId: wrikeTask.Id, title: wrikeTask.Title, description: wrikeTask.Description,
                            status: wrikeTask.Status);
                        syncable.OtherPartyId = task.Id;
                        synchronizationResults.Add(new SynchronizationResult<TaskSyncable>(PartyType.Wrike, SyncState.Updated, syncable) {UpdatedAt = task.UpdatedDate });
                    }
                }
                catch (Exception e)
                {
                    Log.Error(e, $"CreateOrUpdatePartnerTasksOnWrike Id {syncable.OtherPartyId}, PartnerId {syncable.Entity.Id}. Error: {e.Message}.");
                    synchronizationResults.Add(new SynchronizationResult<TaskSyncable>(PartyType.Wrike, SyncState.Error, syncable) { Message = e.Message });
                }
            }

            return synchronizationResults.AsEnumerable();
        }

        private async Task<IEnumerable<SynchronizationResult<CommentSyncable>>> CreateOrUpdatePartnerCommentsOnWrikeAsync(IEnumerable<Syncable<CommentSyncable>> syncables)
        {
            var synchronizationResults = new List<SynchronizationResult<CommentSyncable>>();
            foreach (var syncable in syncables)
            {
                try
                {
                    var wrikeComment = syncable.ToWrikeComment();
                    if (!string.IsNullOrEmpty(wrikeComment.Id)) continue;

                    var comment = await _wrikeClient.Comments.CreateAsync(wrikeComment, true).ConfigureAwait(false);
                    syncable.OtherPartyId = comment.Id;
                    synchronizationResults.Add(new SynchronizationResult<CommentSyncable>(PartyType.Wrike, SyncState.Created, syncable) { UpdatedAt = DateTime.Now });
                }
                catch (Exception e)
                {
                    Log.Error(e, $"CreateOrUpdatePartnerCommentsOnWrike Id {syncable.OtherPartyId}, PartnerId {syncable.Entity.Id}. Error: {e.Message}.");
                    synchronizationResults.Add(new SynchronizationResult<CommentSyncable>(PartyType.Wrike, SyncState.Error, syncable) { Message = e.Message });
                }
            }

            return synchronizationResults.AsEnumerable();
        }
        /*
                private WrikeTask DecorateImportedTask(WrikeTask wrikeTask)
                {
                    string decoration = $"<br /><br /><br />┆Imported from GitLab <a href=\"{issue.Equals()}\">project</a><br /><br /> on {DateTime.Now:f}";,;
                    wrikeTask.Description = $"{wrikeTask.Description}{decoration}";
                    return wrikeTask;
                }
        */
    }
}