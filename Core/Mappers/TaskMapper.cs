﻿using System.Collections.Generic;
using NGitLab.Models;
using Taviloglu.Wrike.Core;

namespace Zhant.Wrike.Integration.GitLab.Core.Mappers
{
    public static class TaskMapper
    {
        public static WrikeTask ToWrikeTask(this Issue issue, string parentId)
        {
            return new WrikeTask
            {
                Title = issue.Title,
                Description = issue.Description, // + $"<br /><br /><br />┆Imported from GitLab <a href=\"{issue.Equals()}\">project</a><br /><br /> on {DateTime.Now:f}";,
                Status = issue.State.ToLowerInvariant() == "closed" ? WrikeTaskStatus.Completed : WrikeTaskStatus.Active,
                CreatedDate = issue.CreatedAt,
                UpdatedDate = issue.UpdatedAt,
                ParentIds = new List<string> {parentId}
            };
        }
    }
}